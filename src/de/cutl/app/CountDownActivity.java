package de.cutl.app;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class CountDownActivity extends Activity implements
        TextToSpeech.OnInitListener {

    public static final String INTENT_MSG_COUNT_DOWN_SECONDS = "INTENT_MSG_COUNT_DOWN_SECONDS";
    public static final String INTENT_MSG_FIRST_SENTENCE = "INTENT_MSG_FIRST_SENTENCE";

    private String firstSentence;
    private TextView secondsView;
    private Thread countDownThread;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_down);

        secondsView = (TextView) findViewById(R.id.countDownSeconds);

        Intent intent = getIntent();
        int seconds = intent.getExtras().getInt(INTENT_MSG_COUNT_DOWN_SECONDS);
        secondsView.setText(String.valueOf(seconds));

        firstSentence = intent.getExtras().getString(INTENT_MSG_FIRST_SENTENCE);
        
        tts = new TextToSpeech(this, this);
    }

    /**
     * Needed for orientation-changes. So the counter counts down and will not
     * be reseted. Will not be called when activity starts normally.
     * 
     * {@link #onSaveInstanceState(Bundle)}
     * {@link #onRestoreInstanceState(Bundle)}
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("test",
                Integer.parseInt(secondsView.getText().toString()));
    }

    /**
     * see {@link #onSaveInstanceState(Bundle)}
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int sec = savedInstanceState.getInt("test");
        secondsView.setText(String.valueOf(sec));
    }

    /**
     * noch keine Ahnung wie ich das testen soll...
     */
    @Override
    protected void onStart() {
        super.onStart();

        countDownThread = new Thread(new Runnable() {
            public void run() {
                speek(firstSentence);
                while (Integer.valueOf(secondsView.getText().toString()) > 0) {

                    secondsView.post(new Runnable() {
                        public void run() {
                            int sec = Integer.valueOf(secondsView.getText()
                                    .toString());
                            --sec;
                            secondsView.setText(String.valueOf(sec));
                            speekToMe(sec);
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Intent result = new Intent("de.cutl.app.RESULT_ACTION", Uri.parse("content://result_uri"));
                setResult(Activity.RESULT_OK, result);
                finish();
            }
        });
    }

    private void speekToMe(int seconds) {

        if (seconds < 5 && seconds > 0) {
            speek(String.valueOf(seconds));
        } else if (seconds == 0) {
            speek("stop");
        }
    }

    private void speek(String value) {
        tts.speak(value, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void onClickCancle(View view) {
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (countDownThread.isAlive()) {
            countDownThread.interrupt();
        }
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    /**
     * After onInit is called the text to speech will work.
     */
    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(Locale.UK);
            tts.setPitch(1f); // height of speech
            tts.setSpeechRate(1f); // speed of speech
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            }
        } else {
            Log.e("TTS", "Initilization Failed!");
        }
        countDownThread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.count_down, menu);
        return true;
    }
}
