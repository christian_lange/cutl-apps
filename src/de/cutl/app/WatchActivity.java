package de.cutl.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class WatchActivity extends Activity {

    private static final int START_COUNTER = 1;

    private Intent intentRound;
    private Intent intentPause;

    private boolean isPause = false;
    private int currentRound = 1;
    private int lastRound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.watch, menu);
        return true;
    }

    public void onClickStart(View view) {
        TextView hoursView = (TextView) findViewById(R.id.hours);
        TextView minsView = (TextView) findViewById(R.id.minutes);
        TextView secsView = (TextView) findViewById(R.id.seconds);
        TextView pauseView = (TextView) findViewById(R.id.pauseSeconds);
        TextView roundsView = (TextView) findViewById(R.id.rounds);

        lastRound = Integer.valueOf(roundsView.getText().toString());
        currentRound = 1;

        int pause = Integer.valueOf(pauseView.getText().toString());

        int countDownSeconds = 3600 * Integer.valueOf(hoursView.getText()
                .toString());
        countDownSeconds += 60 * Integer.valueOf(minsView.getText().toString());
        countDownSeconds += Integer.valueOf(secsView.getText().toString());

        intentRound = new Intent(this, CountDownActivity.class);
        intentRound.putExtra(CountDownActivity.INTENT_MSG_COUNT_DOWN_SECONDS,
                countDownSeconds);

        intentPause = new Intent(this, CountDownActivity.class);
        intentPause.putExtra(CountDownActivity.INTENT_MSG_COUNT_DOWN_SECONDS,
                pause);
        intentPause.putExtra(CountDownActivity.INTENT_MSG_FIRST_SENTENCE,
                "Relax");

        startActivityForResult(intentRound, START_COUNTER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request it is that we're responding to
        if (requestCode == START_COUNTER) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                isPause = !isPause;

                if (isPause) {
                    if (currentRound < lastRound) {
                        ++currentRound;
                        intentRound.putExtra(
                                CountDownActivity.INTENT_MSG_FIRST_SENTENCE,
                                "Round " + currentRound);
                        startActivityForResult(intentPause, START_COUNTER);
                    }
                } else {
                    startActivityForResult(intentRound, START_COUNTER);
                }
            }
        }
    }

    // private boolean isPause = false;
    // private int currentRound = 1;
    // private int lastRound;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean("isPause", isPause);
        outState.putInt("currentRound", currentRound);
    }

    /**
     * see {@link #onSaveInstanceState(Bundle)}
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        isPause = savedInstanceState.getBoolean("isPause");
        currentRound = savedInstanceState.getInt("currentRound");
    }

}
