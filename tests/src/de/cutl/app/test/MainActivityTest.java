package de.cutl.app.test;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.Button;

import de.cutl.app.R;

import de.cutl.app.MainActivity;
import de.cutl.app.WatchActivity;

public class MainActivityTest extends ActivityUnitTestCase<MainActivity> {

	Intent intent;
	Button launchWatch;
	
	public MainActivityTest() {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		intent = new Intent(getInstrumentation().getTargetContext(), MainActivity.class);
		startActivity(intent, null, null);
		launchWatch = (Button) getActivity().findViewById(R.id.launch_watch);
	}
	
    public void testPreconditions() {
    	assertNotNull("intent is null", intent);
    	assertNotNull("launchWatch is null", launchWatch);
    }
	
	public void testLauncher_watch() {
        launchWatch.performClick();

        final Intent launchIntent = getStartedActivityIntent();
        assertNotNull("Intent was null", launchIntent);
        assertTrue(isFinishCalled());
        assertEquals(WatchActivity.class.getName(), launchIntent.getComponent().getClassName());
	}

}
