package de.cutl.app.test;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.Button;
import android.widget.TextView;
import de.cutl.app.CountDownActivity;

public class CountDownActivityTest extends ActivityUnitTestCase<CountDownActivity> {

	Intent intent;
	Button start;
	TextView countDownSeconds;
	
	public CountDownActivityTest() {
		super(CountDownActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}
	
// NullPointer in Zusammenhang mit text to speech	
//	public void testCountdown_10Seconds() {
//		intent = new Intent(getInstrumentation().getTargetContext(), CountDownActivity.class);
//		intent.putExtra(CountDownActivity.INTENT_MSG_COUNT_DOWN_SECONDS, 10);
//		startActivity(intent, null, null);
//		
//		assertNotNull("Activity was null", getActivity());
//
//        TextView seconds = (TextView) getActivity().findViewById(R.id.countDownSeconds);
//        
//        assertEquals("10", seconds.getText().toString());
//	}
	
// NullPointer in Zusammenhang mit text to speech
//	public void testCountdown_60Seconds() {
//		intent = new Intent(getInstrumentation().getTargetContext(), CountDownActivity.class);
//		intent.putExtra(CountDownActivity.INTENT_MSG_COUNT_DOWN_SECONDS, 60);
//		startActivity(intent, null, null);
//		
//		assertNotNull("Activity was null", getActivity());
//
//        TextView seconds = (TextView) getActivity().findViewById(R.id.countDownSeconds);
//        
//        assertEquals("60", seconds.getText().toString());
//	}
	
// NullPointer in Zusammenhang mit text to speech
//	public void testCountdown_cancle() {
//		intent = new Intent(getInstrumentation().getTargetContext(), CountDownActivity.class);
//		intent.putExtra(CountDownActivity.INTENT_MSG_COUNT_DOWN_SECONDS, 60);
//		startActivity(intent, null, null);
//		
//		Button cancle = (Button) getActivity().findViewById(R.id.buttonCancle);
//		cancle.performClick();
//		assertTrue(isFinishCalled());
//	}

}
