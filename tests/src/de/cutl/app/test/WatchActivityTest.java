package de.cutl.app.test;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.Button;
import android.widget.TextView;

import de.cutl.app.R;

import de.cutl.app.CountDownActivity;
import de.cutl.app.WatchActivity;

public class WatchActivityTest extends ActivityUnitTestCase<WatchActivity> {

	Intent intent;
	Button start;
	TextView hours;
	TextView minutes;
	TextView seconds;
	
	public WatchActivityTest() {
		super(WatchActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		intent = new Intent(getInstrumentation().getTargetContext(), WatchActivity.class);
		startActivity(intent, null, null);
		start = (Button) getActivity().findViewById(R.id.buttonStart);
		hours = (TextView) getActivity().findViewById(R.id.hours);
		minutes = (TextView) getActivity().findViewById(R.id.minutes);
		seconds = (TextView) getActivity().findViewById(R.id.seconds);
	}
	
    public void testPreconditions() {
    	assertNotNull("intent is null", intent);
    	assertNotNull("Button start is null", start);
    	assertNotNull("TextView hours is null", hours);
    	assertEquals("00", hours.getText().toString());
    	assertNotNull("TextView minutes is null", minutes);
    	assertEquals("00", minutes.getText().toString());
    	assertNotNull("TextView seconds is null", seconds);
    	assertEquals("10", seconds.getText().toString());
    }
	
	public void testLauncher_watchTimeDefault() {
        start.performClick();

        final Intent launchIntent = getStartedActivityIntent();
        assertNotNull("Intent was null", launchIntent);
        assertFalse(isFinishCalled());
        assertEquals(CountDownActivity.class.getName(), launchIntent.getComponent().getClassName());
        int seconds = launchIntent.getExtras().getInt(CountDownActivity.INTENT_MSG_COUNT_DOWN_SECONDS);
        assertEquals(10, seconds);
	}
	
	public void testLauncher_watchTimeSetted() {
        hours.setText("01");
        minutes.setText("01");
        seconds.setText("01");
        start.performClick();

        final Intent launchIntent = getStartedActivityIntent();
        assertNotNull("Intent was null", launchIntent);
        assertFalse(isFinishCalled());
        assertEquals(CountDownActivity.class.getName(), launchIntent.getComponent().getClassName());
        int seconds = launchIntent.getExtras().getInt(CountDownActivity.INTENT_MSG_COUNT_DOWN_SECONDS);
        assertEquals(1*3600 + 1*60 + 1, seconds);
	}

}
