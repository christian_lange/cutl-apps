Simple Apps for daily use. Only for playing around and to learn more about the Android-Environmen.

I try to run test-driven but its hard in some areas. How I should test text-to-speech?

Infos on how to [start the project](https://bitbucket.org/christian_lange/cutl-apps/wiki/Home)

# Sub-Projects
## Counter
For daily exercises it is usefull to have a count down that speeks to you.

Status: work-in-progress

## Task-Overview
I never found a task-planer that works for me. May be I write my own...

Status: planned

## Life-Tracker
I often tried to write dairies. It never works for a longer period of time. May be a tracking-app is simpler to use and holds the information structed for later analysis.

Status: planned
